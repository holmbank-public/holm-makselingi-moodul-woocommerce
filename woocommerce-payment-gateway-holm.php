<?php
/*
 * Plugin Name:       WooCommerce Liisi financing gateway
 * Plugin URI:        https://www.holmbank.ee/et/api-documentation
 * Description:       Liisi financing payment gateway for WooCommerce.
 * Version:           0.6
 * Author:            Veebipoed, Digiwer, Holm
 * Author URI:        https://www.holmbank.ee/
 * Text Domain:       holm-liisi
 * Domain Path:       languages
 */

if (!defined('ABSPATH')) exit;

if (!class_exists('WC_Holm_Liisi')) {
	define('_HOLM_LIISI_PLUGIN_FOLDER_NAME', 'liisi-financing-woocommerce');

    final class WC_Holm_Liisi {

        protected static $instance = null;

        public $version = '0.2';
        public $name = 'Gateway Liisi Financing';

        private function __construct() {

            add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($this, 'action_links'));
            add_action('init', array($this, 'load_plugin_textdomain'));
            include_once(ABSPATH . 'wp-admin/includes/plugin.php');
			// Check if Woocommerce is active for plugin to work
            if (!is_plugin_active('woocommerce/woocommerce.php')) {
                add_action('admin_notices', array($this, 'woocommerce_missing_notice'));
                return false;
            } else {
				// Check minimum Woocommerce version
                if (defined('WC_VERSION') && version_compare(WC_VERSION, '2.2', '>=')) {
                    if (class_exists('WC_Payment_Gateway')) {
                        $this->includes();
                        add_filter('woocommerce_payment_gateways', array($this, 'add_gateway'));
                        add_action('rest_api_init', array($this, 'register_liisi_callbacks'));
                    }
                } else {
					// Else display notice to user for need of newer Woocommerce version
                    add_action('admin_notices', array($this, 'upgrade_notice'));
                    return false;
                }
            }
        }

		// Register REST API callback routes ( for both _POST and _GET )
        public function register_liisi_callbacks() {
            register_rest_route('liisi_payments', '/callback/(?P<product>[a-zA-Z0-9-]+)', array(
                'methods' => 'POST',
                'callback' => array($this, 'liisi_payments_callback'),
                'permission_callback' => '__return_true',				
            ));
            register_rest_route('liisi_payments', '/callback/(?P<product>[a-zA-Z0-9-]+)', array(
                'methods' => 'GET',
                'callback' => array($this, 'liisi_payments_return'),
				'permission_callback' => '__return_true',
            ));
        }

		// Returns callback URL for order proccesing
        public function liisi_payments_return($request_data) {
			$product = $request_data->get_param('product');
            $url = get_site_url() . "/wc-api/wc_gateway_".$product."_callback?orderId=" . $_GET['orderId'] . "&status=" . $_GET['status'];		
            wp_redirect($url);
            exit;
        }

		// Handles order when Holm posts data
        public function liisi_payments_callback($request_data) {
            global $woocommerce;
            $requestId = $request_data->get_header('x-payment-link-req-id');
            $payload = $request_data->get_params();
            $orderId = $payload['orderId'];
			$order_result = get_order_by_holm_id($orderId);	
			// If order has dissapeared 
            if (empty($order_result)) {
                $this->empty_cart($woocommerce);
                return new WP_Error('invalid_order_id', 'Order not found', array('status' => 404));
            }
			// If somehow more than one order has same holm orderId 
            if (count($order_result) > 1) {
                $this->empty_cart($woocommerce);
                return new WP_Error('invalid_order_id', 'Multiple orders found', array('status' => 400));
            }
            $order_id = $order_result[0];
            $order = wc_get_order($order_id);
            $orderRequestId = $order->get_meta('holm_req_id');
            if (strcmp($orderRequestId, $requestId) !== 0) {
                $this->empty_cart($woocommerce);
                return new WP_Error('invalid_request_id', 'Invalid request id', array('status' => 400));
            }
            $status = $payload['status'];
            $message = __('New Liisi contract event ' . $status . ' received for orderId ' . $orderId, 'holm-liisi');
			$statuses_to_ignore = ['processing', 'completed', 'refunded'];
            if (!in_array($order->get_status(), $statuses_to_ignore)) {
				// Handle different contract outcomes
                switch ($status) {
                    case 'APPROVED':
                        $order->payment_complete();
                        $message = __('Liisi contract signed and paid out. Contract no. ' . $payload['contractNumber'], 'holm-liisi');
                        break;
                    case 'REJECTED':
                        $order->set_status('failed');
                        $message = __('Liisi application rejected for orderId ' . $orderId, 'holm-liisi');
                        break;
                    case 'PENDING':
                        $order->set_status('pending');
                        $message = __('Liisi contract pending for orderId ' . $orderId, 'holm-liisi');
                        break;
                }
            }
            $order->add_order_note($message);
            $order->save();
            $this->empty_cart($woocommerce);
            return null;
        }

        /**
         * @param $woocommerce
         * @return void
         */
        public function empty_cart($woocommerce): void {
            if (!is_null($woocommerce->cart)) {
                $woocommerce->cart->empty_cart();
            }
        }

        public function __clone() {
            _doing_it_wrong(__FUNCTION__, __('Cheatin&#8217; huh?', 'holm-liisi'), $this->version);
        }

        public function __wakeup() {
            _doing_it_wrong(__FUNCTION__, __('Cheatin&#8217; huh?', 'holm-liisi'), $this->version);
        }

        public static function get_instance() {
            if (is_null(self::$instance)) {
                self::$instance = new self();
            }
            return self::$instance;
        }

		// Include required classes
        private function includes() {
            require_once 'includes/class-wc-gateway-liisi.php';
            require_once 'includes/class-wc-gateway-liisi3.php';
            require_once 'includes/class-holm-api.php';
        }

		// Adds direct links to gateway settings in plugins page
        public function action_links($links) {
            if (current_user_can('manage_woocommerce')) {
                $plugin_links = array(
                    '<a href="' . admin_url('admin.php?page=wc-settings&tab=checkout&section=wc_gateway_liisi') . '">' . __('Liisi Financing Settings', 'holm-liisi') . '</a>',
                    '<a href="' . admin_url('admin.php?page=wc-settings&tab=checkout&section=wc_gateway_liisi3') . '">' . __('Liisi3 Financing Settings', 'holm-liisi') . '</a>',
                );
                return array_merge($plugin_links, $links);
            }

            return $links;
        }

		// Loads translations
        public function load_plugin_textdomain() {
            $lang_dir = dirname(plugin_basename(__FILE__)) . '/languages/';
            $lang_dir = apply_filters('woocommerce_holm_languages_directory', $lang_dir);

            $locale = apply_filters('plugin_locale', get_locale(), 'holm-liisi');
            $mofile = sprintf('%1$s-%2$s.mo', 'holm-liisi', $locale);

            $mofile_local = $lang_dir . $mofile;
            $mofile_global = WP_LANG_DIR . '/holm/' . $mofile;

            if (file_exists($mofile_global)) {
                load_textdomain('holm-liisi', $mofile_global);
            } else if (file_exists($mofile_local)) {
                load_textdomain('holm-liisi', $mofile_local);
            } else {
                load_plugin_textdomain('holm-liisi', false, $lang_dir);
            }
        }

		// Notice to show if Woocommerce is not installed
        public function woocommerce_missing_notice() {
            echo '<div class="error woocommerce-message wc-connect"><p>' .
                sprintf(
                    __(
                        'Sorry, <strong>WooCommerce %s</strong> requires WooCommerce to be installed and activated first. Please install <a href="%s">WooCommerce</a> first.',
                        'holm-liisi'
                    ),
                    $this->name,
                    admin_url('plugin-install.php?tab=search&type=term&s=WooCommerce')
                ) . '</p></div>';
        }

		// Notice to show if Woocommerce needs to be updated
        public function upgrade_notice() {
            echo '<div class="updated woocommerce-message wc-connect"><p>' .
                sprintf(
                    __(
                        'WooCommerce %s depends on version 2.2 and up of WooCommerce for this gateway to work! Please upgrade before activating.',
                        'holm-liisi'
                    ), $this->name
                ) . '</p></div>';
        }

		// Add gateways to Woocommerce
        public function add_gateway($methods) {
            $methods[] = 'WC_Gateway_Liisi';
            $methods[] = 'WC_Gateway_Liisi3';
            return $methods;
        }
    }
	
	// Init plugin
	add_action('plugins_loaded', array('WC_Holm_Liisi', 'get_instance'), 0);
	
	// Returns Holm internal codes for finance products or gateway IDs
	function get_liisi_product_code($id = '', $keys = false){
		$codes = [
			'holm_liisi' => 'PRN_HP',
			'holm_liisi3' => 'PRN_LIISI3',
		];
		if(!$keys){
			return $codes[$id] ?: false;
		}else{
			return array_keys($codes);
		}
	}	
	
	// Returns class names for dynamic creation of gateway by ID
	function get_liisi_class($id){
		$codes = [
			'holm_liisi' => 'WC_Gateway_Liisi',
			'holm_liisi3' => 'WC_Gateway_Liisi3',
		];
		return $codes[$id] ?: false;
	}

	// Handles if financing calculator can be shown on single product page
    function liisi_image_on_product_page() {
		$liisi_gateway = new WC_Gateway_Liisi();
		echo "<div class='liisi_wrapper'>";
		if($liisi_gateway->enabled == 'yes' && $liisi_gateway->get_option('product_show_on_page') == 'yes'){
			echo display_liisi_image_product_page($liisi_gateway);
		}		
		$liisi3_gateway = new WC_Gateway_Liisi3();
		if($liisi3_gateway->enabled == 'yes' && $liisi3_gateway->get_option('product_show_on_page') == 'yes'){
			echo display_liisi_image_product_page($liisi3_gateway);
		}
		echo "</div>";
    }
	
	add_action('woocommerce_single_product_summary', 'liisi_image_on_product_page', 6);

    // Remove gateway if needed ( minimum amount not fulfilled or disabled )
	function check_liisi_gateway_availability($gateway, $cart_total){
		$output = true;
		// If gateway enabled is checked and API key is valid
		if($gateway->enabled == 'yes' && !empty($gateway->api_product)){
			$minimum_cart = $gateway->get_option('minimum_cart');
			if(!empty($minimum_cart) && (float)$minimum_cart > 0 && $minimum_cart > $cart_total){
				$output = false;
			}
		}else{
			$output = false;
		}
		return $output;
	}
	
	
	// Handles if gateway is shown in checkout
    function return_liisi_gateways_availability($gateways) {
        global $woocommerce;
		$cart_total = isset(WC()->cart) ? WC()->cart->total : 0;
		
		$liisi_gateway = new WC_Gateway_Liisi();
		$liisi_status = check_liisi_gateway_availability($liisi_gateway, $cart_total);
		if($liisi_status === false){
			unset($gateways[$liisi_gateway->id]);
		}
		
		$liisi3_gateway = new WC_Gateway_Liisi3();
		$liisi3_status = check_liisi_gateway_availability($liisi3_gateway, $cart_total);
		if($liisi3_status === false){
			unset($gateways[$liisi3_gateway->id]);
		}
		return $gateways;			
    }

	add_filter('woocommerce_available_payment_gateways', 'return_liisi_gateways_availability', 10);

	// Loads required CSS asset, JS not used currently
    function load_liisi_assets() {
        //wp_enqueue_script('liisi_js', plugins_url('/js/liisi.js', __FILE__), array('jquery'), false, true);
		wp_enqueue_style('liisi_css', plugins_url('/assets/css/liisi_finance.css', __FILE__));
    }

    add_action('wp_enqueue_scripts', 'load_liisi_assets', 999);
	
	// Returns no logo picture in admin settings
	function get_no_logo(){
		return plugins_url(_HOLM_LIISI_PLUGIN_FOLDER_NAME.'/assets/images/no_logo.png', dirname(__FILE__));
	}
	
	// Dispalys image preview in admin settings
	function do_liisi_admin_image_preview($key = '', $data = '', $gateway){
        $defaults = array(
            'class' => 'button',
            'css' => '',
            'custom_attributes' => array(),
            'desc_tip' => false,
            'description' => '',
            'title' => '',
        );

        $data = wp_parse_args($data, $defaults);
        $widthimage = 200;

        if (!empty($_POST[$key])) {
            $gateway->update_option($key, $_POST[$key]);
        }

        $urlimage = $gateway->get_option($key);
        if ($urlimage)
            $sizeimage = getimagesize($urlimage);
        if (isset($sizeimage) && isset($sizeimage[0]) && $sizeimage [0] < $widthimage) {
            $widthimage = $sizeimage [0];
        }

        ob_start();
        ?>
        <tr valign="top">
            <th scope="row" class="titledesc">
                <label for="<?php echo esc_attr($key); ?>"><?php echo wp_kses_post($data['title']); ?></label>
                <?php echo $gateway->get_tooltip_html($data); ?>
            </th>
            <td class="forminp">
                <img class="holm_admin_logo_preview" src="<?php if ($urlimage)
                    echo $urlimage;
                else
                    echo get_no_logo();
                ?> " style="width:<?php echo $widthimage; ?>px;"/>
                <fieldset>
                    <input type="text" name="<?php echo $key; ?>"
                           value="<?php echo esc_attr($urlimage); ?>"/>
                </fieldset>
                <a href='javascript:void(0);'
                   onclick='jQuery(this).parent().find("input").val("<?php echo $data['default_logo']; ?>");'>Restore
                    logo</a>
            </td>
        </tr>

        <?php
        // ******************* END OF HTML PART ********************
        return ob_get_clean();
	}
	
	// Displays image ( if set ) and product calculation estimation on single product page if enabled
	function display_liisi_image_product_page($gateway = null){
		$product_page_text = '';
		$product_id = get_the_ID();
		$product = wc_get_product($product_id);
		$product_price = wc_get_price_including_tax($product);
		if ($product_price != '') {
			$urlimage = $gateway->get_option('product_payment_logo');
			$intress = (float)$gateway->get_option('product_monthly_intress');
			$periood = (int)$gateway->get_option('holm_periood');
			$min_payment = (float)$gateway->get_option('min_monthly_sum'); //Minimaalne järelmaksu igakuise osamakse suurus on
			$r = $intress / 100;
			$jarelmaks = ($product_price * (1 + $r * $periood / 12)) / $periood;
			$holm_url = $gateway->get_option('product_monthly_payment_url');
			$currency_code = get_woocommerce_currency();
			$currency_symbol = get_woocommerce_currency_symbol();
			if ($jarelmaks < $min_payment) {
				$jarelmaks = $min_payment;
				$product_page_text = '&emsp;';
			} else {
				$jarelmaks_html = wc_price($jarelmaks);
				$product_page_text = sprintf($gateway->get_option('calc_product_page_text'), $jarelmaks_html);
			}
			// Kui arvutuskäik jääb alla 7€, siis kuvatakse "Logo" alates 7€/kuus
			$no_pic_text = $gateway->method_title;
			echo '<div class="liisi_logo">'; 
				if($urlimage && $urlimage != get_no_logo()){
					echo ' <img src="' . $urlimage . '" alt="' . $no_pic_text . '">'.($holm_url ? '<a href="' . $holm_url . '" target="_blank">' : '').'<p> ' . $product_page_text . '</p>'.($holm_url ? '</a>' : '');
				}else{
					echo ' <a href="' . $holm_url . '" target="_blank">' . $no_pic_text . ' <p> ' . $product_page_text . '</p></a>';
				}
			echo "</div>";
		}		
	}
	
	// Handles order after checkout submit
	function check_liisi_payment_response($id){
        global $woocommerce;
        $redirect_to_checkout = false;
        $message = '';
        if (!isset($_REQUEST['orderId']) && !isset($_REQUEST['status'])) {
            $redirect_to_checkout = true;
            $message = __('No response from Holm', 'holm-liisi');
        } else {
			$order_result = get_order_by_holm_id($_REQUEST['orderId']);	
            if (empty($order_result)) {
                $redirect_to_checkout = true;
                $message = __("Didn't find order with that ID!", 'holm-liisi');
            } else {
                $order_id = $order_result[0];
                $order = wc_get_order($order_id);	
                if (!$order || $order->get_payment_method() !== $id) {
                    http_response_code(404);
                    die();
                }
                switch ($_REQUEST['status']) {
                    case 'APPROVED':
                        $return_to = $order->get_checkout_order_received_url();
                        $woocommerce->cart->empty_cart();					
                        $order->payment_complete();
                        break;
                    case 'REJECTED':
                        $redirect_to_checkout = true;
                        $message = __('Holm application rejected or cancelled! Please choose another payment method!', 'holm-liisi');
                        break;
                    case 'PENDING':
                        $order->set_status('pending');
                        $message = __('Holm application in review!', 'holm-liisi');
                        $return_to = $order->get_checkout_order_received_url();
                        $woocommerce->cart->empty_cart();
                        break;
                }				
            }
        }
        if (!empty($message)) {
            wc_add_notice($message, 'notice');
        }
        if ($redirect_to_checkout) {
            wp_redirect(wc_get_checkout_url());
        } else {
            wp_redirect($return_to);
        }
        exit;		
	}
	
	// Returns current language
	function get_liisi_language($locale = ''){
        if (in_array($locale, array('ru', 'ru_RU', 'ru_ru', 'ru-RU', 'ru-ru'))) {
            return 'ru';
        } else if (in_array($locale, array('et', 'ee', 'EE', 'ee_EE', 'ee-EE', 'et_EE', 'et-EE', 'et_ee', 'et-ee'))) {
            return 'et';
        } else {
            return 'en';
        }
    }
	
	// Generates unique order id for order
	function generate_liisi_req_id($order_id = '') {
        return md5(get_site_url() . $order_id . time());
    }
	
	// Returns API product or product image from Holm
	function get_api_product($id, $api){
		$output = false;
		if($id && $api){
			$type = get_liisi_product_code($id);
			$product_request = $api->doCurl('products', [], 'GET');
			if(isset($product_request['data']) && !empty($product_request['data'])){
				foreach($product_request['data'] as $product){
					if($product['type'] == $type){
						$output = $product;
						break;
					}
				}
			}		
		}
		return $output;
	}
	
	// Displays notice if API key is empty or not valid
	function liisi_gateway_api_key_notice(){
		$current_screen = get_current_screen();
		$gateway_ids = get_liisi_product_code('', true);
		// Check if we are on the WooCommerce Liisi gateway settings page
		if($current_screen->id === 'woocommerce_page_wc-settings' && isset($_GET['tab']) && $_GET['tab'] === 'checkout' && isset($_GET['section']) && in_array($_GET['section'], $gateway_ids)){
			$gateway_class_name = get_liisi_class($_GET['section']);
			$gateway = new $gateway_class_name();
			$notice_text = '';
			$payment_key = $gateway->get_option('payment_key');
			if(empty($payment_key)){
				$notice_text = __('Please configure the API key for Liisi financing.', 'holm-liisi');
			}elseif (!$gateway->api_product){
				$notice_text = __('Liisi API key does not seem to be valid!', 'holm-liisi');
			}
			if(!empty($notice_text)){
				?>
				<div class="notice notice-error">
					<p><?php _e($notice_text); ?></p>
				</div>
				<?php
			}
		}
	}
	
	add_action('admin_notices', 'liisi_gateway_api_key_notice');
	
	function get_order_by_holm_id($order_id){
		$args = array(
			'post_type'      => ['shop_order_placehold', 'shop_order'], 
			'post_status' => ['publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash', 
							  'wc-pending', 'wc-processing', 'wc-on-hold', 'wc-completed', 'wc-cancelled', 'wc-refunded', 'wc-failed'], // kas ikka peaks kõigi staatuseid lubama?
			'posts_per_page' => -1,
			'meta_query'     => [
				[
					'key'     => 'holm_order_id',
					'value'   => $order_id,
					'compare' => '=',
				],
			],
		);	
		$query = new WP_Query($args);
		$order_ids = wp_list_pluck($query->posts, 'ID');
		return $order_ids;
	}
	
	function check_gateway_activate_option($gateway){
		if(!$has_been_activated = $gateway->get_option('activated') && $gateway->api_product){
			$gateway->default_logo_pic = $gateway->api_product['logoUrl'];
			$gateway->default_checkout_logo_pic = $gateway->api_product['logoUrl'];
			$options_key = 'woocommerce_'.$gateway->id.'_settings';
			$current_options = get_option($options_key);
			$current_options['product_payment_logo'] = $gateway->api_product['logoUrl'];
			$current_options['checkout_payment_logo'] = $gateway->api_product['logoUrl'];
			$current_options['activated'] = 'yes';
			update_option($options_key, $current_options);
		}
	}
}
