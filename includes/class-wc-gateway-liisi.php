<?php
if (!defined('ABSPATH'))
    exit;

class WC_Gateway_Liisi extends WC_Payment_Gateway {

    public $form_fields = array();
    public $id;	
    public $activated;
    public $enabled;
    public $test_mode;
    public $payment_key;	
    public $do_rounding_product;	
    public $holm_product_code;
	public $api_product;
    public $has_fields;
    public $credit_fields;
    public $method_title;
    public $method_description;
    public $default_logo_pic;
    public $default_checkout_logo_pic;
    public $title;
    public $description;
    public $holm_logo_pic;
    public $checkout_logo_pic;
    public $icon;

    public function __construct() {
		// assign variables
        $this->id = 'holm_liisi';
		$this->activated = $this->get_option('activated');
        $this->enabled = $this->get_option('enabled');
        $this->test_mode = $this->get_option('test_mode');
        $this->payment_key = $this->get_option('payment_key');
		$this->do_rounding_product = $this->get_option('do_rounding_product');		
        $this->holm_product_code = get_liisi_product_code($this->id);
		$this->api_product = get_api_product($this->id, $this->getHolmApi());
        $this->has_fields = false;
        $this->credit_fields = false;
        $this->method_title = __('Liisi järelmaks', 'holm-liisi');
        $this->method_description = __('Liisi järelmaksuga saad ostu eest tasuda paindlike osamaksetena pikema perioodi jooksul.', 'holm-liisi');
		$this->default_logo_pic = !empty($this->api_product) ? $this->api_product['logoUrl'] : get_no_logo();
        $this->default_checkout_logo_pic = !empty($this->api_product) ? $this->api_product['logoUrl'] : get_no_logo();
		$this->title = $this->get_option('display_name') ?: $this->method_title;
		$this->description = $this->get_option('description') ?: $this->method_description;
		$this->holm_logo_pic = $this->get_option('product_monthly_picture') ?: $this->default_logo_pic;
        $this->checkout_logo_pic = $this->get_option('checkout_payment_logo') ?: $this->default_checkout_logo_pic;
		$this->icon = apply_filters('woocommerce_holm_liisi_icon', (get_no_logo() == $this->checkout_logo_pic ? '' : $this->checkout_logo_pic));
		
		// load admin fields
        if (is_admin()) {
			$this->init_form_fields();
			$this->init_settings();			
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
        }
		// placeholders
        add_action('woocommerce_receipt_' . $this->id, array($this, 'receipt_page'));
        add_action('woocommerce_thankyou_' . $this->id, array($this, 'thankyou_page'));
        // register the callback function for when returning from Holm
        add_action('woocommerce_api_wc_gateway_liisi_callback', array($this, 'check_payment_response'));
		// checks when valid API key is saved and then loads gateway images from API product
		add_action('woocommerce_update_options_payment_gateways_'.$this->id, array($this, 'check_activation'));
    }
	
	// Displays settings in admin
    public function init_form_fields() {
        $this->form_fields = array(
            'enabled' => array(
                'title' => __('Enable/Disable', 'holm-liisi'),
                'label' => __('Enable Liisi järelmaks', 'holm-liisi'),
                'type' => 'checkbox',
                'description' => '',
                'default' => 'no'
            ),
            /* Disable test mode setting
            'test_mode' => array(
                'title' => __('Enable test mode', 'holm-liisi'),
                'label' => __('Enable test mode', 'holm-liisi'),
                'type' => 'checkbox',
                'description' => '',
                'default' => 'no'
            ),*/
            'payment_key' => array(
                'title' => __('API key', 'holm-liisi'),
                'type' => 'textarea',
                'description' => '',
                'default' => '',
                'desc_tip' => false
            ),
            'display_name' => array(
                'title' => __('Display Name', 'holm-liisi'),
                'type' => 'text',
                'description' => '',
                'default' => 'Liisi järelmaks',
                'desc_tip' => false
            ),	
			'description' => array(
				'title' => __('Product description', 'holm-liisi'),
				'type' => 'text',
				'description' => __('Product description shown on checkout', 'holm-liisi'),
				'label' => ' ',
				'default' => $this->method_description,
				'desc_tip' => false
			),			
			'product_show_on_page' => array(
				'title' => __('Show image on product page ', 'holm-liisi'),
				'label' => ' ',
				'type' => 'checkbox',
				'description' => '',
				'default' => 'no',
				'desc_tip' => false,
				'class' => "holm_admin_product_fields",
			),
			'minimum_cart' => array(
				'title' => __('Minimum cart amount for financing', 'holm-liisi'),
				'type' => 'number',
				'description' => __('Leave empty for no minimum amount', 'holm-liisi'),
				'custom_attributes' => array('step' => '1'),
				'default' => '',
				'desc_tip' => true
			),			
			'product_monthly_intress' => array(
				'title' => __('Annual interest rate (%)', 'holm-liisi'),
				'type' => 'number',
				'description' => __('Set interest, that number can not more than 100 or less 0', 'holm-liisi'),
				'custom_attributes' => array('min' => '0', 'max' => '100', 'step' => '0.01'),
				'default' => '',
				'desc_tip' => true
			),
			'holm_periood' => array(
				'title' => __('Payment period in months', 'holm-liisi'),
				'type' => 'number',
				'description' => __('Set payment period in months, within 3 to 60', 'holm-liisi'),
				'custom_attributes' => array('min' => '3', 'max' => '60', 'step' => '1'),
				'default' => '60',
				'desc_tip' => false
			),			
			'min_monthly_sum' => array(
				'title' => __('Minimum monthly payment', 'holm-liisi'),
				'type' => 'number',
				'description' => __('Set the minimum monthly payment sum, will be used in product page calculation', 'holm-liisi'),
				'custom_attributes' => array('min' => '7', 'step' => '1'),
				'default' => '7',
				'desc_tip' => false
			),
			'product_monthly_payment_url' => array(
				'title' => __('Product monthly payment url', 'holm-liisi'),
				'type' => 'url',
				'description' => 'For example "https://www.yourdomain.ee/holm-jarelmaks/"',
				'default' => '',
				'desc_tip' => false
			),
			'calc_product_page_text' => array(
				'title' => __('Product page calculator text', 'holm-liisi'),
				'type' => 'text',
				'description' => __('Product page calculator text, keep placeholder %1$s for displaying monthly amount', 'holm-liisi'),
				'default' => __('Monthly payment starting from %1$s', 'holm-liisi'),
				'desc_tip' => false
			),
			'product_payment_logo' => array(
				'title' => __('Image for product page calculator', 'holm-liisi'),
				'type' => 'holmlogo',
				'description' => 'Set the image to use for product page',
				'default' => $this->default_logo_pic,
				'custom_attributes' => array('onclick' => "location.href='http://'",),
				'class' => 'button button-primary',
				'desc_tip' => true,
				'default_logo' => $this->default_logo_pic,
			),
			'checkout_payment_logo' => array(
				'title' => __('Image for payment method at checkout page', 'holm-liisi'),
				'type' => 'holmcheckoutlogo',
				'description' => 'Set the image to use for payment method',
				'default' => $this->default_checkout_logo_pic,
				'custom_attributes' => array('onclick' => "location.href='http://'",),
				'class' => 'button button-primary',
				'desc_tip' => true,
				'default_logo' => $this->default_checkout_logo_pic,
			),
            'do_rounding_product' => array(
                'title' => __('Create temporary product for rounding errors', 'holm-liisi'),
                'label' => __('Will create temporary product amounting to rounding error', 'holm-liisi'),
                'type' => 'checkbox',
                'description' => '',
                'default' => 'no'
            ),
        );
    }

	// Displays product page image
    public function generate_holmlogo_html($key, $data) {
		return do_liisi_admin_image_preview($key, $data, $this);
    }

	// Displays checkout logo
    public function generate_holmcheckoutlogo_html($key, $data) {
		return do_liisi_admin_image_preview($key, $data, $this);
    }

    // Settings field validation start
    public function validate_product_monthly_intress_field($key, $value) {
        if (isset($value) && ($value < 0 || $value > 100)) {
            WC_Admin_Settings::add_error(esc_html__('Looks like you made a mistake with the Intress rate field. Make sure it is not more than 100 or less 0', 'holm-liisi'));
            $value = 0;
        }
        return $value;
    }

    public function validate_holm_periood_field($key, $value) {
        if (isset($value) && ($value < 3 || $value > 60)) {
            WC_Admin_Settings::add_error(esc_html__('Looks like you made a mistake with the payment period. Make sure it is not more than 60 or less 3', 'holm-liisi'));
            $value = 60;
        }
        return $value;
    }
	// Settings field validation end

	// Returns Holm API
    private function getHolmApi() {
        return new Liisi_Api(
            $this->test_mode,
            $this->payment_key
        );
    }

	// Placeholder, not needed
    public function thankyou_page($order_id) {
        //
    }

	// Placeholder, not needed
    public function receipt_page($order_id) {
        //
    }

	// Proccess order when using gateway, adds necessary meta etc
    public function process_payment($order_id) {
        global $woocommerce;

        $order = wc_get_order($order_id);
        $req_id = generate_liisi_req_id($order_id);
        $order->add_order_note(__('Holm: Checkout started', 'holm-liisi'));
        update_post_meta($order_id, 'holm_req_id', $req_id);

		$order_total = number_format($order->get_total(), 2, '.', '');
		$items_total = 0.0; // Komakohaga arv
        $order_data = [
            'paymentType' => $this->holm_product_code,
            'totalAmount' => $order_total,
            'orderNumber' => $order_id,
            'returnUrl' => get_site_url() . "/wp-json/liisi_payments/callback/liisi"
        ];

        $wpml_customer_lng = apply_filters('wpml_current_language', null);
        if ($wpml_customer_lng) {
            $order_data['language'] = get_liisi_language($wpml_customer_lng);
        } else {
            $order_data['language'] = get_liisi_language(get_locale());
        }

        foreach ($order->get_items() as $item_id => $item) {
            $product = wc_get_product((isset($item['variation_id']) && !empty($item['variation_id']) ? $item['variation_id'] : $item['product_id']));
            $sku = $product->get_sku();
			$item_total = number_format($item->get_total() + $item->get_total_tax(), wc_get_price_decimals(), '.', '');
			$items_total = $items_total+$item_total;
            $order_data['products'][] = [
                'productSKU' => $sku,
                'productName' => $item->get_name(),
                'quantity' => $item->get_quantity(),
                'totalPrice' => $item_total,
            ];
			
        }

        foreach ($order->get_shipping_methods() as $item_id => $item) {
            $shipping = $item->get_data();
			$shipping_total = number_format($shipping['total'] + $shipping['total_tax'], wc_get_price_decimals(), '.', '');
			$items_total = $items_total+$shipping_total;
            $order_data['products'][] = [
                'productSKU' => 'SHIP',
                'productName' => $shipping['name'],
                'quantity' => 1,
                'totalPrice' => $shipping_total,
            ];
        }
		 
		if($this->do_rounding_product == 'yes'){
			$order_amount_diff = 0.0;
			$epsilon = 0.00001;
			if (abs($order_total - $items_total) < $epsilon) {
				$order_amount_diff = 0.0;
			} elseif ($order_total > $items_total) {
				$order_amount_diff = $order_total - $items_total;
			} else {
				$order_amount_diff = $items_total - $order_total;
			}
			if ($order_amount_diff > 0) {
				$order_data['products'][] = [
					'productSKU' => 'ROUNDING',
					'productName' => 'ROUNDING',
					'quantity' => 1,
					'totalPrice' => number_format($order_amount_diff, 2, '.', ''),
				];				
			}		
		}

        $api = $this->getHolmApi();
        $send_order = $api->doCurl('order', $order_data, 'POST', $req_id);
        if (isset($send_order['data']['orderId'])) {
            update_post_meta($order_id, 'holm_order_id', $send_order['data']['orderId']);
            $output = array(
                'result' => 'success',
                'redirect' => $send_order['data']['orderLink'],
            );
        } else {
            wc_add_notice(__("Couldn't send order to Holm!", 'holm-liisi'), 'notice');
            $output = array(
                'result' => 'fail',
                'redirect' => wc_get_checkout_url(),
            );
        }

        return $output;
    }
	
	// Callback after checkout submit
	public function check_payment_response(){
		check_liisi_payment_response($this->id);
	}
	
	// When valid api key inserted activates plugin and loads logos from api
	public function check_activation(){
		check_gateway_activate_option($this);
	}
}

?>
