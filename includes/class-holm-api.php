<?php

class Liisi_Api {
    const TEST_URL = 'https://gateway-partner.hbc.nonprod.holmbank.ee/api/partners/public/payment-link/';
    const LIVE_URL = 'https://gwp.holmbank.ee/api/partners/public/payment-link/';
	
    const LIVE = false;

    protected $mode;

    protected $url;

    public function __construct($mode, $payment_key) {
        $this->mode = $mode;
        $this->payment_key = $payment_key;
        $this->setUrl();
    }

    protected function setUrl() {
        if (filter_var($this->mode, FILTER_VALIDATE_BOOLEAN) === self::LIVE) {
            $this->url = self::LIVE_URL;
        } else {
            $this->url = self::TEST_URL;
        }
		//$this->url = 'https://wiremocks.nonprod.holmbank.ee/api/partners/public/payment-link/';
    }

    public function getActionUrl($action = '') {
        switch ($action) {
            case 'products':
                return $this->url . "products";
            case 'order':
                return $this->url . "orders";
        }
        return $this->url;
    }

    public function doCurl($action = '', $post = [], $type = '', $req_id = '') {
        $full_request_url = $this->getActionUrl($action);
        $headers = array(
            'Content-Type: application/json',
            'x-payment-link-key: ' . $this->payment_key,
        );
        if (!empty($req_id)) {
            $headers[] = 'x-payment-link-req-id: ' . $req_id;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $full_request_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, true);
        if (!empty($post)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
        }

        $response = curl_exec($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $header_size);
        $body = substr($response, $header_size);
        return ['header' => $header, 'data' => json_decode($body, true)];
    }
}
